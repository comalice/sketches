import io
import pytest
import pytest_use_stdin.pytest_use_stdin as p

def test_prompt_name(monkeypatch):
    monkeypatch.setattr('sys.stdin', io.StringIO('Albert'))
    assert p.prompt_name() == "Albert"

def test_multi_prompt(monkeypatch):
    monkeypatch.setattr('sys.stdin', io.StringIO('this\nis\nsparta\n'))
    assert p.multi_prompt() == ['this', 'is', 'sparta']
